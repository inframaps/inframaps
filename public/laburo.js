/******************************************************************************

                            Online Java Compiler.
                Code, Compile, Run and Debug java program online.
Write your code in this editor and press "Run" button to execute it.

*******************************************************************************/
import java.math.BigInteger;
public class Main
{
     final static BigInteger M = new BigInteger("2017");
     
	public static void main(String[] args) {
	for (long n : new long[] { 1L, 2L, 5L, 10L, 20L, 58184241583791680L }) {
        String s = "";
        for (long i = 0; i < n; i++) 
        {
            s = s + n;
        }
        System.out.println(s);
        BigInteger x = new BigInteger(s.toString()).mod(M);

System.out.println("" + n + ": " + x);

}

}

}
1
1: 1
22
2: 22
55555
5: 1096
10101010101010101010
10: 1197
2020202020202020202020202020202020202020
20: 57
^C

