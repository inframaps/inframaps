const express = require('express');  
const logger = require('morgan');
const path = require('path');
//var log4js = require('log4js');
const fs = require('fs');


const mongoose = require('mongoose')
const bodyParser = require('body-parser');
//const userModel = require('../src/models/users');
console.log("importando la conexion a la base de datos")
const mongoose1 = require('../config/database'); //Importando la configuracion de conexion a la BD

mongoose1.connection.on('error', console.error.bind(console, 'Error de conexion en MongoDB'))
.then(db => console.log("Conectado a la base de datos"))
.catch(err => console.log("error1"))



/////////////////////////////////////////////////

// log4js.configure({
//     appenders: {
//       console: { type: 'console' },
//       file: { type: 'file', filename: 'app.log' }
//     },
//     categories: {
//       cheese: { appenders: ['file'], level: 'info' },
//       default: { appenders: ['console'], level: 'info' },
//       full:{appenders:['console', 'file'], level: 'info' }
//     }
//    });
//    var logger = log4js.getLogger('full');
//Starting the server

   /////////////////////////////////////////////////

const app = express();

const port1=3000;
console.log(port1)
const path1 = path.join(__dirname,'views');
console.log(path1)

const indexRoute=require('./routes/index');


// app.set('port', process.env.PORT || port1);
app.set('port', port1);
app.set('views', path1);
app.set('view engine','ejs');
console.log("seguimos bien por acá")
app.use(logger('dev'));     //morgan other options: dev/combined/tiny/short/common
// app.use(log4js.connectLogger(logger, { level: 'info' })); //definir si utilizar morgan u log4js!!!

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true})); // otra forma! app.use(express.urlencoded({extended: false})); // https://stackoverflow.com/questions/60006174/express-and-nodejs-object-null-prototype
app.use('/', indexRoute);
// app.get('/', function(req, res){
//  res.json({"tutorial" : "Construyendo una API REST con NodeJS"});
// });


app.listen(app.get('port'), ()=>{
    console.log("escuchando en el puerto", port1)
});

const dir_public="D:\\Programacion\\WEB\\solo_usuarios\\public";
console.log(dir_public)
app.use(express.static(dir_public ))

// let rawdata = fs.readFileSync('student.json');
// let student = JSON.parse(rawdata);

/* try {
    
    userModel.create(        { user: "lalal", empresa: "cvsingenieria", email: "aadsaddsda", nombre: "juan prez", rol: "cualquiera", password: "xaasd123" });
console.log("............................creado!");
}
catch (err) {
    console.log(err)
}
    */
