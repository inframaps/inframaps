// Cargamos el módulo de mongoose
const mongoose = require('mongoose');
// Cargamos el módulo de bcrypt
// const bcrypt = require('bcrypt');
// // Definimos el factor de costo, el cual controla cuánto tiempo se necesita para calcular un solo hash de BCrypt. Cuanto mayor sea el factor de costo, más rondas de hash se realizan. Cuanto más tiempo sea necesario, más difícil será romper el hash con fuerza bruta.
// const saltRounds = 10;
//Definimos los esquemas
const Schema = mongoose.Schema;
// Creamos el objeto del esquema con sus correspondientes campos

const pointSchema = new mongoose.Schema({
    type: {
      type: String,
      enum: ['Point','MultiPoint', 'LineString', 'MultiLineString', 'Polygon', 'MultiPolygon', 'GeometryCollection'],
      required: true
    },
    coordinates: {
      type: Array,
      required: true
    }
  });

const objectSchema = new Schema({
 name: {
  type: String,
  trim: true,  
  required: true,
 },
 geometry: {
    type: pointSchema,
    required: true
  },
  properties: {
    type: Array
  },

 ID: {
     type: Number,
     required: true,
 },

 parentID: {
    type: Number,
    required: true,
},
order: {
    type: Number,
    required: true,
}
})
// Antes de almacenar la contraseña en la base de datos la encriptamos con Bcrypt, esto es posible gracias al middleware de mongoose
// UserSchema.pre('save', function(next){
//   this.password = bcrypt.hashSync(this.password, saltRounds);
//   next();
// });
// Exportamos el modelo para usarlo en otros ficheros


const Object = mongoose.model('Object', objectSchema);
const Point = mongoose.model('Point', pointSchema);

module.exports = {
    Object: Object,
    Point : Point
}